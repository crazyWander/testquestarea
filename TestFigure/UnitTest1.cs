using System;
using NUnit.Framework;
using testQuestArea;

namespace TestFigure
{

    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [TestCase(5, ExpectedResult = 78.5398)]
        [TestCase(2, ExpectedResult = 12.5664)]
        [TestCase(1, ExpectedResult = 3.1416)]
        public decimal TestRingArea(int a)
        {
            Round testRound = new Round(a);
            var area = testRound.getArea();
            return (decimal)Math.Round(area, 4);
        }

        [TestCase(5, 3, 4, ExpectedResult = 6)]
        [TestCase(6, 3, 4, ExpectedResult = 5.3327)]
        [TestCase(7,5,8,  ExpectedResult = 17.3205)]
        public decimal TestTriangleArea(int a, int b, int c)
        {
            Triangle testTriangle = new Triangle(a,b,c);
            return (decimal)Math.Round(testTriangle.getArea(),4);
        }

        [TestCase(5, 3, 4, ExpectedResult = true)]
        [TestCase(6, 3, 4, ExpectedResult = false)]
        [TestCase(7, 5, 8,  ExpectedResult = false)]
        public bool isRightTriangle(int a, int b, int c)
        {
            Triangle testTriangle = new Triangle(a, b, c);
            return testTriangle.rigthTrianrle();
        }

        [Test]
        public void ArgumentExeptionTriangle_negative()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Triangle testTriangle = new Triangle(5, -3, 4);
            });
        }
        
        public void ArgumentExeptionTriangle_Zero()
        {
            Assert.Throws<ArgumentException>(() =>
            {
            Triangle testTriangle = new Triangle(0, 0, 0);
            });
        }
    }
}