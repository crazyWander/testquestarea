﻿using System;

namespace testQuestArea
{
    public interface Figure
    {
        public double getArea();
        
        /*
        /// <summary>
        /// Вычисление площади фигуры без знания типа фигуры в compile-time
        /// Реализовал бы так, но 
        /// В дальнейшем труднее модифицировать, трудно читать и дебажить
        /// </summary>
        /// <param name="par">Длины сторон или радиуса</param>
        /// <returns></returns>
        /// <exception cref="Exception">если задано больше 3 длин сторон или null</exception>
        public double getArea(params int[] par)
        {
            double Res;
            switch(par.Length)
            {
                case 1:
                {
                    Res = Math.PI * par[0] * par[0];
                    return Res;
                }
                case 2:
                {
                    Res = par[0] * par[1];
                    return Res;
                }
                case 3:
                {
                    double p = 0;
            
                    foreach (var VARIABLE in par)
                    {
                        p += VARIABLE;
                    }

                    p /= 2;
                    var area = p * (p - par[0]) * (p - par[1]) * (p - par[2]);
                    area = Math.Sqrt(area);
                    return area;
                }
                default:
                    throw new Exception("метод может расчитывать тольк круги, треугольники и прямоугольники");
            }
        }*/
    }
    
    
    public class Round : Figure
    {
        private int _radius;

        public Round(int r)
        {
            if (r > 0)
                _radius = r;
            else throw new ArgumentException("Радиус не может быть отрицательным или меньше нуля");
        }

        public double getArea()
        {
            return Math.PI * _radius * _radius;
        }
    }

    public class Triangle : Figure
    {
        private double[] _part = new double[3];

        public Triangle(params int[] line)
        {
            for (int i = 0; i < 3; i++)
                if (line[i]>0)
                    _part[i] = line[i];
                else
                {
                    throw new ArgumentException("Стороны не могут быть отрицательными или равны 0");
                }
        }

        public double getArea()
        {
            double p = 0;
            
            foreach (var VARIABLE in _part)
            {
                p += VARIABLE;
            }

            p /= 2;
            var area = p * (p - _part[0]) * (p - _part[1]) * (p - _part[2]);
            area = Math.Sqrt(area);
            return area;
        }

        public bool rigthTrianrle()
        {
            double p = 0;
            double max = 0;
            
            foreach (var VARIABLE in _part)
            {
                p += VARIABLE * VARIABLE;
                if (VARIABLE > max)
                    max = VARIABLE;
            }

            double piphagorTest = p - max * max;

            return Math.Sqrt(piphagorTest) == max;
        }
    }
    
}