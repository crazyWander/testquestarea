create table category (
    id INT primary key,
    name char(20) not NULL
);

insert into category
values 
(1, 'ретро игры'),
(2, 'рогалик'),
(3, '3D'),
(4, '2D');

create table product (
    id INT primary key,
    name char(200) not NULL
);

insert into product
values 
(1, 'mario'),
(2, 'dead cells'),
(3, 'rogue like'),
(4, 'rick of rain');

create table ProductAndCategory(
product_id int not null,
    category_id int not null
);

insert into productandcategory
values
(1,1),
(1,4),
(2,2),
(2,4),
(3,1),
(3,2),
(4,2),
(4,3),
(4,4);

select product.name, category.name
from product
left join ProductAndCategory pc on product.id = pc.product_id
inner join category on category.id = pc.category_id
order by product.name;